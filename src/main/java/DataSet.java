import java.util.*;

public class DataSet implements Iterable <Data>{
    private List<Data> list;

    public DataSet(){}

    public DataSet(List<Data> list) {
        this.list = list;
    }

    public void addData(Data elem) {
        this.list.add(elem);
    }

    @Override
    public Iterator<Data> iterator() {
        return list.iterator();
    }

    public void sort(){
        list.sort(Comparator.comparing(Data::getName));
    }
}
