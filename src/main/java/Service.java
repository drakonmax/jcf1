import java.util.*;

public class Service{

    public static Collection<Data> nameEqualsList(List<Data> inputList, String name){
        List<Data> resList = new ArrayList<>();
        for (Data elem : inputList) {
            if (elem.getName().equals(name))
                resList.add(elem);
        }
        return resList;
    }

    public static Collection<Data> compareWithLevel(List<Data> inputList, double level){
        List<Data> resList = new ArrayList<>();
        for (Data elem : inputList) {
            if (Math.abs(elem.getValue())<=level)
                resList.add(elem);
        }
        return resList;
    }

    public static Set<Double> namesInList(List<Data> inputList, Set<String> name){
        Set<Double> resSet = new HashSet<>();
        for (Data elem : inputList) {
            if (name.equals(elem.getName()))
                resSet.add(elem.getValue());
        }
        return resSet;
    }

    public static String[] positiveValue(List<Data> inputList){
        Set<String> uniqueNames = new HashSet<>();
        for (Data elem : inputList) {
            if (elem.getValue() > 0)
                uniqueNames.add(elem.getName());
        }
        return (String[]) uniqueNames.toArray();
    }

    public static <T> Set<T> unity(List<Set<T>> list)
    {
        Set<T> res = new HashSet<>();
        for (Set<T> elem : list) {
            res.addAll(elem);
        }
        return res;
    }

    public static <T> Set<T> intersection(List<Set<T>> list)
    {
        Set<T> res = new HashSet<>(list.get(0));
        for (int i=1; i<list.size(); i++) {
            res.retainAll(list.get(i));
        }

        return res;
    }

    public static <T> List<Set<T>> maxSizeSets(List<Set<T>> list)
    {
            List<Set<T>> res = new ArrayList<>();
            int max=0;
            for (Set<T> elem : list) {
                if (elem.size() > max)
                {
                    res.clear();
                    max = elem.size();
                    res.add(elem);
                }
                else if (elem.size() == max)
                    res.add(elem);
            }
            return res;
    }
}
